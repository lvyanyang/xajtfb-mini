const config = require('./config')
const request = require('./request')

//全局数据
const globalData = {
  autoLogined: false, //是否已经自动登录过了
  sessionInfo: null,  //当前微信账号会话信息,包括:openid-用户唯一标识 session_key-会话密钥 unionid-用户在开放平台的唯一标识符
  userProfile: null,  //当前微信账号用户昵称头像信息,包括:avatarUrl city country gender(0-未知 1-男性 2-女性) language nickName province
  tokenInfo: null     //用户登录后的Token信息
}

//region User

/**
 * 获取当前小程序 appId
 * @return {string}
 */
function getAppId() {
  return wx.getAccountInfoSync().miniProgram.appId
}

/**
 * 获取用户信息。每次请求都会弹出授权窗口，用户同意后返回 userInfo。
 * @param desc {string} 声明获取用户个人信息后的用途，不超过30个字符
 */
function getUserProfile(desc) {
  if (globalData.userProfile) {
    return Promise.resolve(globalData.userProfile)
  }
  return new Promise((resolve, reject) => {
    wx.getUserProfile({
      lang: 'zh_CN',
      desc: desc,
      success(res) {
        globalData.userProfile = res.userInfo
        console.log(res.userInfo)
        resolve(res.userInfo)
      },
      fail(err) {
        reject(err)
        console.error('wx.getUserProfile fail', err)
      }
    })
  })
}

/**
 * 获取用户会话信息
 * @return {Promise<Object>}
 */
function getSessionInfo() {
  if (globalData.sessionInfo) {
    return Promise.resolve(globalData.sessionInfo)
  }

  return new Promise((resolve, reject) => {
    wx.login({
      success(res) {
        const code = res.code
        const appid = wx.getAccountInfoSync().miniProgram.appId
        request.post(config.code2SessionApiUrl, {appid, code}, {loading: '准备登录...'}).then(r => {
          globalData.sessionInfo = r.data
          resolve(r.data)
        }).catch(e => {
          reject(e)
        })
      },
      fail(err) {
        console.error('wx.login fail', err)
      }
    })
  })
}

/**
 * 获取用户身份标识符(openid)
 * @return {Promise<Object>}
 */
function getOpenid() {
  if (globalData.sessionInfo) {
    return Promise.resolve(globalData.sessionInfo.openid)
  }
  const openid = wx.getStorageSync('openid')
  if (openid) {
    return Promise.resolve(openid)
  }

  return new Promise((resolve, reject) => {
    getSessionInfo().then(res => {
      resolve(res.openid)
    }).catch(reject)
  })
}

/**
 * 获取用户会话key
 * @return {Promise<Object>}
 */
function getSessionKey() {
  if (globalData.sessionInfo) {
    return Promise.resolve(globalData.sessionInfo.sessionKey)
  }

  return new Promise((resolve, reject) => {
    getSessionInfo().then(res => {
      resolve(res.sessionKey)
    }).catch(reject)
  })
}

/**
 * 获取手机号码
 * @param encryptedData 包括敏感数据在内的完整用户信息的加密数据
 * @param iv 加密算法的初始向量
 * @return {Promise<Object>}
 */
function getPhoneNumber(encryptedData, iv) {
  return new Promise((resolve, reject) => {
    getSessionKey().then(res => {
      const sessionKey = res
      const appid = wx.getAccountInfoSync().miniProgram.appId
      request.post(config.phoneNumberApiUrl, {appid, sessionKey, encryptedData, iv}, {loading: '读取中...'}).then(r => {
        resolve(r.data)
      }).catch(reject)
    }).catch(reject)
  })
}

/**
 * 用户注册并返回登录
 * @return {Promise<Object>}
 */
function register(registerUserInfo) {
  return new Promise((resolve, reject) => {
    request.post(config.registerApiUrl, registerUserInfo, {loading: '正在注册...'}).then(res => {
      _loginSuccess(res.data)
      resolve(res.data)
    }).catch(reject)
  })
}

/**
 * 用户登录获取token,并把用户信息[userInfo]存入Storage中
 * @return {Promise<Object>}
 */
function login() {
  return new Promise((resolve, reject) => {
    getOpenid().then(res => {
      const openid = res
      request.post(config.loginApiUrl, {openid}, {loading: '正在登录...'}).then(r => {
        globalData.autoLogined = true
        _loginSuccess(r.data)
        resolve(r.data)
      }).catch(reject)
    }).catch(reject)
  })
}

function _loginSuccess(tokenInfo) {
  if (!tokenInfo) return
  globalData.tokenInfo = tokenInfo
  wx.setStorageSync('tokenInfo', tokenInfo)
}

/**
 * 获取用户Token信息
 * @return {Promise<Object>}
 */
function getTokenInfo() {
  if (globalData.tokenInfo) {
    return globalData.tokenInfo
  }
  return wx.getStorageSync('tokenInfo') || null
}

/**
 * 如果当前已登录成功返回true,否则返回false
 */
function isLogin() {
  if (globalData.tokenInfo) {
    return Promise.resolve(true)
  }
  const tokenInfo = wx.getStorageSync('tokenInfo')
  if (tokenInfo) {
    globalData.tokenInfo = tokenInfo
    return Promise.resolve(true)
  }
  if (globalData.autoLogined === false) {
    return new Promise((resolve, reject) => {
      login().then(res => {
        resolve(!!res)
      }).catch(reject)
    })
  }
  return Promise.resolve(false)
}

/**
 * 登录拦截器,如果当前未登录,则跳转到登录页面
 * @return {Promise}
 */
function loginInterceptor() {
  return new Promise((resolve, reject) => {
    isLogin().then(r => {
      if (r === true) {
        resolve({})
      } else {
        wx.navigateTo({
          url: config.registerPagePath
        })
      }
    }).catch(reject)
  })
}

//endregion

module.exports = {
  register,
  login,
  isLogin,
  loginInterceptor,
  getUserProfile,
  getSessionInfo,
  getTokenInfo,
  getAppId,
  getOpenid,
  getSessionKey,
  getPhoneNumber
}
