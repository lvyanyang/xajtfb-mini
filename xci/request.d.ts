// @ts-ignore
import IAnyObject = WechatMiniprogram.IAnyObject;
// @ts-ignore
import RequestOption = WechatMiniprogram.RequestOption;

/**
 * 网络请求自定义选项
 */
interface XCIRequestOption extends Partial<RequestOption> {
    /** 是否显示网络请求加载提示,如果为null或者false则不显示,如果为字符串则显示此提示内容,默认值:加载中... */
    loading?: string | boolean,
    /** 是否添加config中的PageSize参数,默认值:false */
    pageSize?: boolean
}

/**
 * 分页结果对象
 */
interface PageDataResult {
    /** 当前页码 */
    pageIndex: number,
    /** 每页记录数 */
    pageSize: number,
    /** 当前记录开始索引 */
    startIndex: number,
    /** 当前记录结束索引 */
    endIndex: number,
    /** 记录总数 */
    total: number,
    /** 总页数 */
    totalPage: number,
    /** 是否有数据 */
    hasData: boolean,
    /** 是否有上一页 */
    previousPage: boolean,
    /** 是否有下一页 */
    nextPage: boolean,
    /** 是否首页 */
    firstPage: boolean,
    /** 是否最后一页 */
    lastPage: boolean,
    /** 数据集合 */
    rows: Array<any>
}

/**
 * 网络请求结果
 */
interface XCIRequestResult {
    /** 网络请求状态，true表示请求调用成功，false表示请求调用失败。 */
    code: number,
    /** 网络请求状态码，大于等于0请求调用成功，小于0表示请求调用失败。 */
    success: boolean,
    /** 请求消息，多用来返回错误消息。 */
    msg?: string,
    /** 请求相关的业务数据，data可以是数字、字符串、日期、对象、集合以及可序列化的所有对象。 */
    data?: string | Array<any> | IAnyObject
}

/**
 * 网络请求结果
 */
interface XCIRequestPageResult {
    /** 网络请求状态，true表示请求调用成功，false表示请求调用失败。 */
    code: number,
    /** 网络请求状态码，大于等于0请求调用成功，小于0表示请求调用失败。 */
    success: boolean,
    /** 请求消息，多用来返回错误消息。 */
    msg?: string,
    /** 请求相关的业务数据，data可以是数字、字符串、日期、对象、集合以及可序列化的所有对象。 */
    data: PageDataResult
}

/**
 * GET网络请求
 * @param url 请求接口地址
 * @param data 请求数据
 * @param setting 请求设置
 */
export declare function get(url: string, data?: IAnyObject, setting?: XCIRequestOption): Promise<XCIRequestResult>

/**
 * POST网络请求
 * @param url 请求接口地址
 * @param data 请求数据
 * @param setting 请求设置
 */
export declare function post(url: string, data?: IAnyObject, setting?: XCIRequestOption): Promise<XCIRequestResult>