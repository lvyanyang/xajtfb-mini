const request = require('./request')

/** 客户服务 */
const customerService = {
  /**
   * 根据主键查询单个客户档案
   * @param id {string} 客户主键
   * @return {Promise<Object>}
   */
  selectById: (id) => request.post(`/api/om/basicCustomer/selectById/${id}`),

  /**
   * 查询客户档案列表
   * @param data {Object} [data={}] 请求参数
   * @return {Promise<Object>}
   */
  selectPageList: (data = {}) => request.post(`/api/om/basicCustomer/selectPageList`, data, {pageSize: true}),
}

/** 车辆服务 */
const vehicleService = {
  /**
   * 根据主键查询单个车辆档案
   * @param id {string} 车辆主键
   * @return {Promise<Object>}
   */
  selectById: (id) => request.post(`/api/om/basicVehicle/selectById/${id}`),

  /**
   * 查询车辆档案列表
   * @param data {Object} [data={}] 请求参数
   * @return {Promise<Object>}
   */
  selectPageList: (data = {}) => request.post(`/api/om/basicVehicle/selectPageList`, data, {pageSize: true}),
}

/** 公交服务 */
const busService = {
  /**
   * 根据主键查询单个公交线路档案
   * @param id {string} 公交线路主键
   * @return {Promise<Object>}
   */
  selectBusLineById: (id) => request.post(`/api/bus/line/selectById`, {id}),

  /**
   * 查询公交线路档案列表
   * @param data {Object} [data={}] 请求参数
   * @return {Promise<Object>}
   */
  selectBusLinePageList: (data = {}) => request.post(`/api/bus/line/selectPageList`, data, {pageSize: true}),


  /**
   * 查询交通资讯列表
   * @param data {Object} [data={}] 请求参数
   * @return {BoolResult<Object>}
   */
  selectWxNewsinformationPageList: (data = {}) => request.post(`/api/bus/wxNewsinformation/selectPageList`, data, {pageSize: true}),

  /**
   * 查询单个交通资讯
   * @param data {Object} [data={}] 请求参数
   * @return {BoolResult<Object>}
   */
  selectWxNewsinformationById: (data = {}) => request.post(`/api/bus/wxNewsinformation/selectById`, data, {pageSize: false}),
   /**
   * 查询视频列表
   * @return {BoolResult<Object>}
   */
  selectVideosInfoList: () => request.post(`/api/bus/wxNewsinformation/selectVideosInfo`, null, {pageSize: false}),

  /**
   * 新增评价
   * @param data {Object} [data={}] 请求参数
   * @return {Promise<Object>}
   */
  insertWxEvaluation: (data = {}) => request.post(`/api/bus/wxevaluation/insert`, data, {pageSize: true}),

  /**
   * 出租投诉信息表服务
   * @param data {Object} [data={}] 请求参数
   * @return {Promise<Object>}
   */
  insertWxComplaint: (data = {}) => request.post(`/api/bus/wxComplaint/insert`, data, {pageSize: true}),

  /**
   * 查询出租投诉信息表分页列表
   * @param data {Object} [data={}] 请求参数
   * @return {Promise<Object>}
   */
  selectWxComplaintPageList: (data = {}) => request.post(`/api/bus/wxComplaint/selectPageList`, data, {pageSize: true}),

  /**
   * 通过坐标信息查询公交站点信息
   * @param data {Object} [data={}] 请求参数
   * longitude  经度
   * latitude 纬度
   * distance 距离
   * @return {Promise<Object>}
   */
  getStationByLonglat: (data = {}) => request.post(`/api/bus/stationInfo/getStationByLonglat`, data),

  /**
   * 通过查询条件查询 公交站点
   */
  selectStationInfoList: (data = {}) => request.post(`/api/bus/stationInfo/selectList`, data),

  /**
   * 获取实时公交数据
   * @param data {Object} [data={}] 请求参数
   * @return {Promise<Object>}
   */
  getRealBusLine: (routeId) => request.post(`/api/bus/intelligentAdjustable/getRealBusLine`, {routeId}),

}

/** 维修处服务 */
const wxcService = {
  /**
   * 根据主键查询单个驾校档案
   * @param id {string} 驾校主键
   * @return {Promise<Object>}
   */
  selectSchoolById: (id) => request.post(`/api/wxc/school/selectById`, {id}),

  /**
   * 查询驾校档案列表
   * @param data {Object} [data={}] 请求参数
   * @return {Promise<Object>}
   */
  selectSchoolPageList: (data = {}) => request.post(`/api/wxc/school/selectPageList`, data, {pageSize: true}),

  /**
   * 根据主键查询单个教练员基本资料表
   * @param id {string} 教练员主键
   * @return {Promise<Object>}
   */
  selectSchCoachById: (id) => request.post(`/api/wxc/schCoach/selectById`, {id}),

  /**
   * 查询教练员基本资料表分页列表
   * @param data {Object} [data={}] 请求参数
   * @return {Promise<Object>}
   */
  selectSchCoachPageList(data = {}) {
    return new Promise((resolve, reject) => {
      setTimeout(function () {
        request.post(`/api/wxc/schCoach/selectPageList`, data, {pageSize: true}).then(resolve).catch(reject)
      }, 2000)
    })
  },

  /**
   * 根据主键查询单个训练场基本资料表
   * @param id {string} 教练员主键
   * @return {Promise<Object>}
   */
  selectSchGrounpById: (id) => request.post(`/api/wxc/schGrounp/selectById`, {id}),

  /**
   * 查询训练场基本资料表分页列表
   * @param data {Object} [data={}] 请求参数
   * @return {Promise<Object>}
   */
  selectSchGrounpPageList: (data = {}) => request.post(`/api/wxc/schGrounp/selectPageList`, data, {pageSize: true}),

  /**
   * 根据主键查询单个教练车基本资料
   * @param id {string} 教练员主键
   * @return {Promise<Object>}
   */
  selectSchVehicleById: (id) => request.post(`/api/wxc/schVehicle/selectById`, {id}),

  /**
   * 查询教练车基本资料分页列表
   * @param data {Object} [data={}] 请求参数
   * @return {Promise<Object>}
   */
  selectSchVehiclePageList: (data = {}) => request.post(`/api/wxc/schVehicle/selectPageList`, data),
}

/** 维修处服务 */
const gjznService = {
  /**
   * 查询车辆最后Gps位置
   * @param routeId {string} 线路Id
   * @param busIds {string} 车辆Ids字符串,多个逗号隔开
   * @return {Promise<Object>}
   */
  selectBusLastGpsPosition: (routeId, busIds) => request.post(`/api/gjzn/selectBusLastGpsPosition`, {routeId, busIds}),

  /**
   * 查询车辆Gps轨迹
   * @param busId {string} 车辆Id
   * @param startTime {string} 开始时间
   * @param endTime {string} 结束时间
   * @return {Promise<Object>}
   */
  selectBusGpsTrack: (busId, startTime, endTime) => request.post(`/api/gjzn/selectBusGpsTrack`, {busId, startTime, endTime}),
}

module.exports = {
  customer: customerService,
  vehicle: vehicleService,
  bus: busService,
  wxc: wxcService,
  gjzn: gjznService
}