/** 配置参数 */
const Config = {
  /** 服务器基地址 */
  //baseUrl: 'https://lyy.xa96716.cn',
  baseUrl: 'http://localhost:7001',
  /** 默认每页数量 */
  pageSize: 20,
  /** 本地注册绑定页面 */
  registerPagePath: '/pages/register2/register2',
  /** 登录凭证校验服务器接口地址 */
  code2SessionApiUrl: '/wxmini/code2Session',
  /** 获取手机号码服务器接口地址 */
  phoneNumberApiUrl: '/wxmini/phoneNumber',
  /** 用户注册服务器接口地址 */
  registerApiUrl: '/api/mini/user/register',
  /** 用户登录服务器接口地址 */
  loginApiUrl: '/api/mini/user/login',
  /** 文件上传接口地址 */
  uploadFileApiUrl: '/file/upload',
  /** 是否显示网络请求加载提示,如果为null或者false则不显示,如果为字符串则显示此提示内容,默认值:加载中... */
  requestDefaultLoading: '加载中...',
  /** 加载提示延迟毫秒数,默认:500ms */
  requestLoadingDelay: 500,

  /** 获取token */
  getToken() {
    return ''
  },
  /**
   * 生成接口完整请求URL
   * @param url {string} 接口路径
   * @return {string} 返回完整请F求URL
   */
  getApiUrl(url) {
    if (url.indexOf('://') === -1) {
      url = this.baseUrl + url
    }
    return url
  },
  /**
   * 全局错误处理
   * 返回 true表示此错误已被处理,request底层将不再reject错误
   * 返回 false|null|undefined 表示未处理的异常,request会reject错误
   * @param err
   */
  handleError(err) {
    console.error(err)
    if (__wxConfig.envVersion === 'develop') {
      let errorMsg = ''
      if (err instanceof String) {
        errorMsg = err.toString()
      } else if (err.msg) {
        errorMsg = err.msg
      }
      wx.showModal({
        title: '系统提示',
        content: errorMsg,
        showCancel: false
      })
    }
    return true
  },
  /** 获取默认网络请求请求头 */
  getDefaultHeader() {
    return {
      appId: '20210210',
      token: this.getToken(),
      timestamp: new Date().getTime(),
      // 'content-type': 'application/x-www-form-urlencoded',
      // 'content-type': 'application/json',
    }
  },
}

/** 环境配置参数 */
const EvnConfig = {
  //开发版配置
  develop: {},
  //体验版配置
  trial: {},
  //发布版配置
  release: {}
}

/**
 * 微信环境配置参数
 * @typedef {Object} __wxConfig
 * @property {'develop'|'trial'|'release'} envVersion - 环境版本
 * @property {Object} networkTimeout - 各类网络请求的超时时间，单位均为毫秒
 * @property {number} networkTimeout.request - wx.request 的超时时间，默认值：60000，单位：毫秒。
 * @property {number} networkTimeout.connectSocket - wx.connectSocket 的超时时间，默认值：60000，单位：毫秒。
 * @property {number} networkTimeout.uploadFile - wx.uploadFile 的超时时间，默认值：60000，单位：毫秒。
 * @property {number} networkTimeout.downloadFile - wx.downloadFile 的超时时间，默认值：60000，单位：毫秒。
 */
Object.assign(Config, EvnConfig[__wxConfig.envVersion])

module.exports = {
  ...Config
}