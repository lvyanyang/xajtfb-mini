const pagingBehavior = require('../../../../xci/pagingBehavior')
const service = require('../../../../xci/service')
// const PagingDataLoad = require('../../../../utils/PagingDataLoad')
// const paging = new PagingDataLoad(service.wxc.selectSchCoachPageList);

Page({
  behaviors: [pagingBehavior],
  /**
   * 页面的初始数据
   */
  data: {
    showModalStatus: false,
    schCoach: {
      schoolName: null,
      name: null,
      quasiTeachCategory: null,
      quasiTeachTypeName: null
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad(options) {
    await this.loadData()
    wx.lin.initValidateForm(this)
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  async onPullDownRefresh() {
    await this.loadData()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  async onReachBottom() {
    await this.loadData(false)
  },

  /**
   * 加载数据
   * @param firstPage 是否加载第一页数据
   */
  async loadData(firstPage = true) {
    await this.loadPageData(service.wxc.selectSchCoachPageList, {schCoach: this.data.schCoach}, firstPage)
  },

  async submit(event) {
    const {detail} = event
    this.setData({schCoach: detail.values})
    await this.loadData()
    this.onHidePupopTap()
  },

  onBack() {
    wx.navigateBack({
      delta: 1,
    })
  },
  onShowPupopTap() {
    this.setData(
            {showModalStatus: true}
    )
  },
  onHidePupopTap() {
    this.setData(
            {showModalStatus: false}
    )
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})