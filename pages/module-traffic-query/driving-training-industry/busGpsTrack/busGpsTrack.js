const config = require('../../../../xci/config')
Page({
  data: {
    gpsTrackFilter: {
      busId:"148569",
      startTime:"2021-03-29 08:00:00",
      endTime:"2021-03-29 09:00:00"
    },
    marker: [], // 初始点
    markers: [], // 标记点集合
    polyline: [], // 坐标点集合
    rows:[],
    i: 0, // 用于循环，
    isCoutinue:true,
  },
  stop(){
    this.setData({
      isCoutinue:false
    })
  },
  start(){
    this.setData({
      isCoutinue:true
    }),
        this.translateMarker(this.data.markers);
  },
  end(){
    this.setData({
      isCoutinue:false,
      i: 0
    })
    this.translateMarker(this.data.marker);
  },

  getBusGpsTrack(){
    let that = this;
    wx.request({
      url: config.getApiUrl('/api/gjzn/selectBusGpsTrack'),
      data: this.data.gpsTrackFilter,
      method: "post",
      success: function (res) {
        that.setData({
          rows:res.data.data,
          i: 0
        })
        that.data.rows.map((value, index) => {
          that.data.rows[index].width = 20;
          that.data.rows[index].height = 20;
          that.data.rows[index].id = 1;
          that.data.rows[index].iconPath = '../busGpsTrack/image/movecar.png';
        });

        that.setData({
          polyline: [{
            points: that.data.rows,
            color: '#2a88f5',
            width: 10,
            arrowLine:true
          }],
          isCoutinue:true,
          "marker[0]": that.data.rows[0],
          markers: that.data.rows,
          latitude: that.data.rows[0].latitude,
          longitude: that.data.rows[0].longitude
        })
        that.MapContext.includePoints({
          points:that.data.rows,
          padding:[30]
        })
        that.translateMarker(that.data.rows);
      }
    })
  },

  submit(event){
    const {detail} = event;
    this.setData({
      gpsTrackFilter:detail.values
    })
    this.getBusGpsTrack();
  },

  onLoad: function() {
    let that = this;
    wx.lin.initValidateForm(this);
    wx.getSystemInfo({
      success: function (res) {
        //设置map高度，根据当前设备宽高满屏显示
        that.setData({
          view: {
            Height: res.windowHeight
          }
        })
      }
    })

    // 获取当前坐标
    wx.getLocation({
      type: 'wgs84',
      success: (res) => {
        this.setData({
          latitude: res.latitude,
          longitude: res.longitude
        })

      }
    })
  },
  // 平移marker，带动画
  translateMarker: function(markers) {
    let that = this;
    let markerId = markers[that.data.i].id;
    if(this.data.i+1<markers.length){
      let destination = {
        longitude: markers[that.data.i + 1].longitude,
        latitude: markers[that.data.i + 1].latitude
      };
      if(this.data.isCoutinue){
        this.MapContext.translateMarker({
          markerId: markerId,
          destination: destination,
          autoRotate: true,
          rotate: '180',
          moveWithRotate:true,
          duration: 2,
          success(res) {
            that.setData({
              i: that.data.i + 1
            });
            // 小于长度减1才执行
            if (that.data.i < markers.length - 1) {
              that.translateMarker(markers);
            }
          },
          fail(err) {
            console.log('fail', err)
          }
        })
      }
    }
  },
  onReady: function (e) {
    this.MapContext = wx.createMapContext('map')
  }
})
