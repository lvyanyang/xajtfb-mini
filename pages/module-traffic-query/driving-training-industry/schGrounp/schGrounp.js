const pagingBehavior = require('../../../../xci/pagingBehavior')
const service = require('../../../../xci/service')


Page({
  behaviors: [pagingBehavior],
  /**
   * 页面的初始数据
   */
  data: {
    showModalStatus: false,
    schGroup:{
      schoolName:null,
      groundAddress:null
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad(options) {
    await this.loadData();
    wx.lin.initValidateForm(this);
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  async onPullDownRefresh() {
    await this.loadData();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  async onReachBottom() {
    await this.loadData(false);
  },

  /**
   * 加载数据
   * @param firstPage 是否加载第一页数据
   */
  async loadData(firstPage = true) {
    await this.loadPageData(service.wxc.selectSchGrounpPageList, this.data.schGroup, firstPage)
  },

  async submit(event){
    const {detail} = event;
    console.log(detail);
    this.setData(  
      {schGroup : detail.values}
    );
    await this.loadData();
    this.onHidePupopTap();
  },
  
  onBack() {
    wx.navigateBack({
      delta: 1,
    })
  },
  onShowPupopTap() {
    this.setData(  
      {showModalStatus: true}  
    );
  },
  onHidePupopTap(){
    this.setData(  
      {showModalStatus: false}  
    );
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})