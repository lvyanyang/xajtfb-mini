Page({
  data: {
    items: [
      {title: '驾校查询', url: '/pages/module-traffic-query/driving-training-industry/school/school'},
      {title: '驾校教练员信息查询', url: '/pages/module-traffic-query/driving-training-industry/schCoach/schCoach'},
      {title: '驾校教练车信息查询', url: '/pages/module-traffic-query/driving-training-industry/schVehicle/schVehicle'},
      {title: '驾校训练场信息查询', url: '/pages/module-traffic-query/driving-training-industry/schGrounp/schGrounp'},
    ]
  }
});