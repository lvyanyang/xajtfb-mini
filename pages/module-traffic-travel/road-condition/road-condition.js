// const amapFile = require('../../../utils/amap-wx.js');
// const myAmapFun = new amapFile.AMapWX({key:'9b6148a7e00e0ebb4037cd7f455b1930'});
const helper = require('../../../xci/helper')

Page({
  data: {
    latitude: '',
    longitude: '',
  },

  async onLoad() {
    await helper.authorize("scope.userLocation", '需要获取您的地理位置，请确认授权，否则地图功能将无法使用');
    const result = await wx.getLocation({});
    console.log(result);
    this.setData({
      latitude: result.latitude,
      longitude: result.longitude
    });


    // const self = this;
    // wx.getLocation({
    //   success(res) {
    //     self.setData({
    //       latitude: res.latitude,
    //       longitude: res.longitude
    //     });
    //   },
    //   fail(err) {
    //     wx.getSetting({
    //       success: function (res) {
    //         if (res.authSetting["scope.userLocation"] === false) {
    //           wx.showModal({
    //             title: '是否授权当前位置',
    //             content: '需要获取您的地理位置，请确认授权，否则地图功能将无法使用',
    //             success: function (tip) {
    //               if (tip.confirm) {
    //                 wx.openSetting({
    //                   success: function (data) {
    //                     if (data.authSetting["scope.userLocation"] === true) {
    //                       wx.showToast({
    //                         title: '授权成功',
    //                         icon: 'success',
    //                         duration: 1000
    //                       })
    //                       //授权成功之后，再调用chooseLocation选择地方
    //                       wx.getLocation({
    //                         success: function (res) {
    //                           self.setData({
    //                             latitude: res.latitude,
    //                             longitude: res.longitude
    //                           });
    //                         },
    //                       })
    //
    //                     } else {
    //                       wx.showToast({
    //                         title: '授权失败',
    //                         icon: 'success',
    //                         duration: 1000
    //                       })
    //                     }
    //                   }
    //                 })
    //               }else{
    //                 wx.showToast({
    //                   title: '拒绝授权',
    //                   icon: 'success',
    //                   duration: 1000
    //                 })
    //               }
    //             }
    //           })
    //         }
    //       },
    //       fail: function (res) {
    //         wx.showToast({
    //           title: '获取用户当前权限设置出错',
    //           icon: 'success',
    //           duration: 1000
    //         })
    //       }
    //     })
    //   }
    // });

  },
})