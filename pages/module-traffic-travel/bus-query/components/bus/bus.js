// pages/module-traffic-travel/bus-realtime/components/bus/bus.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    bus: {
      type: Object,
      value: null
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    direction:true
  },

  /**
   * 组件的方法列表
   */
  methods: {
    switchType(){
      this.setData({
        direction:!this.data.direction
      })
    },
    switch(e){
      // console.log(e.currentTarget.dataset.name)
      let stationName = e.currentTarget.dataset.name;
      //触发父组件中的自定义事件 同时传递数据给父组件（stationName）  
      this.triggerEvent("transmitStationName", {
        stationName
      });
    }
  },
  

  // observers: {
  //   'bus': function (params) {//  'params'是要监听的字段，（params）是已更新变化后的数据

  //   console.log(this.properties.bus);
  //     // this.setData({
  //     //   params2: params //这里params只能赋值给params2（另外一个字段），不然赋值给params自己就会陷入死循环，导致内存占用过高，开发者工具死机。
  //     // })
  //   }
  // },
})
