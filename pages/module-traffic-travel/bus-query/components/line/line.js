// pages/module-traffic-travel/bus-realtime/components/line/line.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    bus: {
      type: Object,
      value: null
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    direction:true
  },

  /**
   * 组件的方法列表
   */
  methods: {
    //切换上下行
    switchType(){
      this.setData({
        direction:!this.data.direction
      })
    },

    //跳转到实时公交查询
    handleSwitch(){ 
      this.triggerEvent("switchRealBus");
    },
    
    //点击站点跳转到附近站点查询
    switch(e){
      // console.log(e.currentTarget.dataset.name)
      let stationName = e.currentTarget.dataset.name;
      //触发父组件中的自定义事件 同时传递数据给父组件（stationName）  
      this.triggerEvent("transmitStationName", {
        stationName
      });
    }
  }
  
})
