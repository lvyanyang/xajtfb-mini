const chooseLocation = requirePlugin('chooseLocation');

Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    position:{
      latitude : '',  
      longitude : ''  
    },
    imgs: {
			rightArrow: `https://3gimg.qq.com/lightmap/xcx/demoCenter/images/iconArrowRight@3x.png`
    },
    startPoint: {
			name: '',
			latitude: '',
			longitude: ''
		},
		endPoint: null,
    markers: [{
      callout: {
        content: '当前位置',
        padding: 10,
        borderRadius: 2,
        display: 'ALWAYS'
      },
      latitude: '',
      longitude: '',
      iconPath: '/static/Marker1_Activated@3x.png',
      width: '34px',
      height: '34px',
      rotate: 0,
      alpha: 1
    }]
  },

  /**
   * 组件的方法列表
   */
  methods: {
    onChooseStartPoint () {
      var that = this;
      wx.chooseLocation({
        success(res){
          that.setData({
            startPoint: {
              name:res.name,
              latitude:res.latitude,
              longitude:res.longitude
            }
          });
        }
      })
    },
    onChooseEndPoint () {
      var that = this;
      wx.chooseLocation({
        success(res){
          that.setData({
            endPoint: {
              name:res.name,
              latitude:res.latitude,
              longitude:res.longitude
            }
          });
        }
      })
    },
    onWatchDemo () {
      if (!this.data.endPoint) {
        wx.showToast({
          title: '请选择终点位置',
          icon: 'none',
          duration: 1500,
          mask: false
        });
        return;
      }
      const endPoint = JSON.stringify(this.data.endPoint);
      const startPoint = this.data.startPoint ? JSON.stringify(this.data.startPoint) : '';
      let url = 'plugin://routePlan/index?key=2TEBZ-XFTRX-WFC45-ZYIBA-XNBV2-SPFP6&referer=腾讯位置服务示例中心小程序' + '&endPoint=' + endPoint +
		  '&mode=transit' ;
      if (startPoint) {
        url += '&startPoint=' + startPoint;
      }
      wx.navigateTo({
        url
      });
    },
  },
  attached: function(){
    wx.getLocation({
      type: 'wgs84',
      success: (res) => {
        this.setData({
          position:{
            latitude : res.latitude, // 纬度
            longitude : res.longitude // 经度
          },
          'markers[0].latitude' :res.latitude, 
          'markers[0].longitude' : res.longitude ,
          startPoint:{
            name:'当前位置',
            latitude : res.latitude, // 纬度
            longitude : res.longitude // 经度
          },
        })
      }
    })
  },
})
