const { bus } = require('../../../xci/service');
const service = require('../../../xci/service')
const select = service.bus.getRealBusLine;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    bus:null,
    stationName:null,
    imagesTabs: [{
      tab: '实时公交',
      key: '0',
      picPlacement: 'top',
      image: {
        activeImage: '/pages/module-traffic-travel/bus-query/images/bus-selected.png',
        defaultImage: '/pages/module-traffic-travel/bus-query/images/bus.png',
      }
    },
    {
      tab: '线路查询',
      key: '1',
      picPlacement: 'top',
      image: {
        activeImage: '/pages/module-traffic-travel/bus-query/images/line-selected.png',
        defaultImage: '/pages/module-traffic-travel/bus-query/images/line.png',
      }
    }, {
      tab: '换乘查询',
      key: '2',
      picPlacement: 'top',
      image: {
        activeImage: '/pages/module-traffic-travel/bus-query/images/transfer-selected.png',
        defaultImage: '/pages/module-traffic-travel/bus-query/images/transfer.png',
      }
    },
    {
      tab: '附近站点',
      key: '3',
      picPlacement: 'top',
      image: {
        activeImage: '/pages/module-traffic-travel/bus-query/images/site-selected.png',
        defaultImage: '/pages/module-traffic-travel/bus-query/images/site.png',
      }
    }
    ],
    index:0
  },

  //搜索框搜索事件
  clickrouteid(e){
    select(e.detail.routeid).then(res => {
      this.setData({
        bus:res.data
      })
    });
  },

  //实时公交、线路查询、点击站点切换到附近站点查询页
  transmitStationName(e){
    this.setData({
      index:3,
      stationName:e.detail.stationName
    })
  },

  //线路查询页中点击线路名称切换到实时公交页面
  switchRealBus(){
    this.setData({
      index:0
    })
  },

  //附近站点页中点击线路切换到实时公交页面
  stationInfoClickrouteid(e){
    //console.log("点击线路",e.detail.routeid);
    this.setData({
      index:0
    })
    select(e.detail.routeid).then(res => {
      this.setData({
        bus:res.data
      })
    });
  }
})