const service = require('../../../xci/service')

Page({
  data: {
    title: '交通出行',
    links: [
      {title: '查公交', icon: 'https://www.xadjtfb.com/Assets/images/cGjIco.png', url: '/pages/webview/index?url=https://www.xadjtfb.com/BusPage/bus_realtime'}, ///pages/module-traffic-travel/bus-query/bus-query
      //{title: '叫出租', icon: 'https://www.xadjtfb.com/Assets/images/jczIco.png', url: '/pages/webview/index?url=http%3a%2f%2fwww.didapinche.com%2ftaxiapp%3fsourcecid%3dtaxi_h5_xian%26clientid%3dweixin_h5'},
    ],
    items: [
      {title: '客票预订', icon: 'https://www.xadjtfb.com/Assets/images/kpydIco.png', url: '/pages/prompt/index'},
      {title: '机场大巴', icon: 'https://www.xadjtfb.com/Assets/images/jcdbIco.png', url: '/pages/module-traffic-travel/airport-bus/airport-bus'},
      //{title: '交通资讯', icon: 'https://www.xadjtfb.com/Assets/images/xxfbIco.png', url: ''},
      {title: '地铁出行', icon: 'https://www.xadjtfb.com/Assets/images/dtxxIco.png', url: '/pages/webview/index?url=https://www.xadjtfb.com/SecDirectory/Subway'},
      {title: '路网信息', icon: 'https://www.xadjtfb.com/Assets/images/lwxxIco.png', url: '/pages/webview/index?url=http://map.xianjiaojing.com:11180/xianwx/'},
      {title: '实时路况', icon: 'https://www.xadjtfb.com/Assets/images/sslkIco.png', url: '/pages/webview/index?url=https://www.xadjtfb.com/www/road/lukuang.htm'}, ///pages/module-traffic-travel/road-condition/road-condition
      {title: '通勤班车', icon: 'https://www.xadjtfb.com/Assets/images/tqbc.png', url: '/pages/webview/index?url=https://www.xadjtfb.com/CommuterTrain/bus_realtime'},
    ],
    videos:[
      {title: '测试', icon: 'https://www.xadjtfb.com/Assets/images/kpydIco.png', url: 'http://wxsnsdy.tc.qq.com/105/20210/snsdyvideodownload?filekey=30280201010421301f0201690402534804102ca905ce620b1241b726bc41dcff44e00204012882540400&bizid=1023&hy=SH&fileparam=302c020101042530230204136ffd93020457e3c4ff02024ef202031e8d7f02030f42400204045a320a0201000400'},
      {title: '客票预订', icon: 'https://www.xadjtfb.com/Assets/images/kpydIco.png', url: 'http://wxsnsdy.tc.qq.com/105/20210/snsdyvideodownload?filekey=30280201010421301f0201690402534804102ca905ce620b1241b726bc41dcff44e00204012882540400&bizid=1023&hy=SH&fileparam=302c020101042530230204136ffd93020457e3c4ff02024ef202031e8d7f02030f42400204045a320a0201000400'},
      {title: '测试', icon: 'https://www.xadjtfb.com/Assets/images/kpydIco.png', url: 'http://wxsnsdy.tc.qq.com/105/20210/snsdyvideodownload?filekey=30280201010421301f0201690402534804102ca905ce620b1241b726bc41dcff44e00204012882540400&bizid=1023&hy=SH&fileparam=302c020101042530230204136ffd93020457e3c4ff02024ef202031e8d7f02030f42400204045a320a0201000400'},
      {title: '客票预订', icon: 'https://www.xadjtfb.com/Assets/images/kpydIco.png', url: 'http://wxsnsdy.tc.qq.com/105/20210/snsdyvideodownload?filekey=30280201010421301f0201690402534804102ca905ce620b1241b726bc41dcff44e00204012882540400&bizid=1023&hy=SH&fileparam=302c020101042530230204136ffd93020457e3c4ff02024ef202031e8d7f02030f42400204045a320a0201000400'}
    ],
    newsinfosrows:[
      {
        "id": 19,
        "infoclass": "238路线路调整公告",
        "newsinfo": "经上级部门批准同意，238路将于2017年元月8日，由西华门缩线至张家堡西（盐张村）。",
        "adddate": "2017-01-10 18:44:23",
        "roadstate": null,
        "weather": null,
        "emergency": null,
        "classs": "新闻资讯",
        "addoper": null,
        "releasesign": null,
        "releaseoper": null,
        "releasedate": null
      },
      {
        "id": 16,
        "infoclass": "春节期间ETC通行温馨提示",
        "newsinfo": "2017年春节期间7座以下（含7座）小型客车免费通行高速公路时间为：1月27日零点开始，至2月2日24点结束，共7天。免收通行费时间以车辆驶离高速公路出口收费车道的时间为准。",
        "adddate": "2017-01-10 18:27:40",
        "roadstate": null,
        "weather": null,
        "emergency": null,
        "classs": "新闻资讯",
        "addoper": null,
        "releasesign": null,
        "releaseoper": null,
        "releasedate": null
      }
    ]
  },
  onLoad: function (options) {
    
    this.loadVideosData();
    this.loadWxNewsinformationData()
  },
  loadWxNewsinformationData:function(){
    service.bus.selectWxNewsinformationPageList({
      pageIndex: 1,
      pageSize: 10,
      classs: "新闻资讯",
    }).then(res => {
      //根据输入的数据去查--》渲染到下面
      //console.log(res);
      this.setData({
        newsinfosrows: res.data.rows
      })
    })
  },
  loadVideosData:function(){
    service.bus.selectVideosInfoList().then(res => {
      //根据输入的数据去查--》渲染到下面
      //console.log(res);
      this.setData({
        videos: res.data
      })
    })
  }
});
