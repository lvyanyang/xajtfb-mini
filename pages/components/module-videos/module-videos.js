
Component({
  externalClasses: [
    'title-class'
  ],
  properties: {
    videos: {
      type: Array,
      value: null
    },
  },
  lifetimes: {
    attached: function() {
    }
  },
  data: {},
  methods: {
  }
});
