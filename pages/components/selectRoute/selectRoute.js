const service = require('../../../xci/service')

let timer;
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    // 搜索内容
    search_content: "",
    //访问后台对象
    search: {
      key: ""
    },
    // 搜索内容文字长度
    search_content_length: 0,
    // 历史搜索线路列表
    historySearchRouteList: [],
    // 去后台查询到的线路
    inquireRouteList: [],
    //显示多少条（超过这个数字  显示一个按钮（显示更多按钮））
    showSize:5,
    // 是否显示 加载更多按钮
    isShowDisplaymore:false
  },

  lifetimes: {
    attached() {
      let historySearchRouteList = wx.getStorageSync('historySearchRouteList');
      if (historySearchRouteList && historySearchRouteList.length > 0) {
        this.setData({
          historySearchRouteList
        })
      }
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    //  输入框的值改变 就会触发的事件
    handleInput(e) {
      // 1 获取输入框的值
      let search_content = e.detail.value.replace(/\s+/g, '');
      if (search_content.length > 0) {
        this.setData({
          search_content: search_content,
          search_content_length: 1
        })
        if(timer) clearTimeout(timer);
        timer = setTimeout(()=>{this.playSearch()}, 500);
      } else {
        this.setData({
          search_content: search_content,
          search_content_length: 0,
          inquireRouteList:[],
          showSize:5,
          isShowDisplaymore: false
        })
      }
    },
    // 点击搜索文字
    playSearch() {
      let search_content = this.data.search_content;
      if (search_content == '') {
        return;
      } else {
        service.bus.selectBusLinePageList({
          pageIndex: 1,
          pageSize: 1000,
          key: search_content
        }).then(res => {
          //根据输入的数据去查--》渲染到下面
          this.setData({
            inquireRouteList: res.data.rows,
            showSize:5,
            isShowDisplaymore:res.data.rows.length >5 ? true: false
          })
        })
      }
    },

    displaymore(e){
      this.setData({
        isShowDisplaymore:false,
        showSize :this.data.inquireRouteList.length
      })
    },
    // 点击取消按钮
    endSearch() {
      wx.navigateBack({
        delta: 1
      })
    },
    // 点击删除搜索记录
    delectHistorySearchRouteList(e) {
      wx.showModal({
        title: "删除记录",
        content: "你确定要删除搜索记录吗?",
        success: (res) => {
          if (res.confirm) {
            wx.removeStorageSync('historySearchRouteList');
            this.setData({
              historySearchRouteList: []
            })
          }
        }
      })
    },
    // 点击搜索历史
    clickSearchHistorical(e) {
      //在搜索历史里面 选中线路
      let routeid = e.currentTarget.dataset.routeid;
      //触发父组件中的自定义事件 同时传递数据给父组件（routeid）  
      this.triggerEvent("clickrouteid", {
        routeid
      });
    },
    //点击线路
    clickInquireRoute(e) {
      const {
        routeid,
        name
      } = e.currentTarget.dataset;
      //把当前点击的存入缓存中
      let historySearchRouteList = this.data.historySearchRouteList;
      if (historySearchRouteList.length != 0) {
        historySearchRouteList.map((item, index) => {
          if (item.name == name) {
            historySearchRouteList.splice(index, 1)
          }
        })
      }
      historySearchRouteList.unshift({
        routeid: routeid,
        name: name
      });
      this.setData({
        historySearchRouteList: historySearchRouteList,
        inquireRouteList:[]
      })
      wx.setStorageSync('historySearchRouteList', this.data.historySearchRouteList);
      //触发父组件中的自定义事件 同时传递数据给父组件（routeid）  
      this.triggerEvent("clickrouteid", {
        routeid
      });
    }
  }
})