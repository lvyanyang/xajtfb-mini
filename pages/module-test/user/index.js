const auth = require('../../../xci/auth')
let userInfo = {}

Page({
  data: {
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
  },
  async onLoad(options) {
    await auth.loginInterceptor()
    // const result = await helper.getOpenId()
    // userInfo = result.data
  },
  async getUserProfile() {
    // const userInfo = await auth.getUserInfo('是否同意获取用户信息')
    // console.log(userInfo)
  },

  bindGetUserInfo: function () {
    wx.navigateTo({
      url: '/pages/auth/index'
    })
  },

  async getPhoneNumber(e) {
    if (!e.detail.encryptedData || !e.detail.iv) return
    const result = await auth.getPhoneNumber(e.detail.encryptedData, e.detail.iv)
    console.log('手机号=', result.data)
  },

})

// // 公共登录动作
//   doLogin: function (callback) {
//     // let that = this
//     // wx.login({
//     //   success: function (loginRes) {
//     //     console.log(loginRes, 'loginRes')
//     //     if (loginRes.code) {
//     //       /*
//     //        * @desc: 获取用户信息 期望数据如下
//     //        *
//     //        * @param: userInfo       [Object]
//     //        * @param: rawData        [String]
//     //        * @param: signature      [String]
//     //        * @param: encryptedData  [String]
//     //        * @param: iv             [String]
//     //        **/
//     //       wx.getUserInfo({
//     //         // withCredentials: true, // 非必填, 默认为true
//     //
//     //         success: function (infoRes) {
//     //           console.log('infoRes:', infoRes)
//     //           // 请求服务端的登录接口
//     //
//     //           wx.request({
//     //             // url: `${config.baseUrl}/wx/user/mylogin`,
//     //             url: `${config.baseUrl}/wx/user/login`,
//     //             method: 'POST',
//     //             data: {
//     //               code: loginRes.code,
//     //               rawData: infoRes.rawData, // 用户非敏感信息
//     //               signature: infoRes.signature, // 签名
//     //               encryptedData: infoRes.encryptedData, // 用户敏感信息
//     //               iv: infoRes.iv // 解密算法的向量
//     //             },
//     //
//     //             success: function (res) {
//     //               console.log('login success:', res)
//     //               //sessionKey = res.data.sessionKey;
//     //               that.showInfo('成功')
//     //               // res = res.data;
//     //               // if (res.success) {
//     //               //   that.globalData.userInfo = res.module.userInfo;
//     //               //   console.log(
//     //               //       "globalData.userInfo",
//     //               //       that.globalData.userInfo
//     //               //   );
//     //               //   wx.setStorageSync("userInfo", res.module.userInfo);
//     //               //   wx.setStorageSync("loginFlag", res.module.token);
//     //               //   if (callback) {
//     //               //     callback();
//     //               //   }
//     //               // } else {
//     //               //   that.showInfo(res.errMsg);
//     //               // }
//     //             },
//     //
//     //             fail: function (error) {
//     //               // 调用服务端登录接口失败
//     //               that.showInfo('调用服务端登录接口失败')
//     //               console.log(error)
//     //             },
//     //           })
//     //         },
//     //
//     //         fail: function (error) {
//     //           // 获取 userInfo 失败，去检查是否未开启权限
//     //           //wx.hideLoading();
//     //           that.showInfo('获取 userInfo 失败，去检查是否未开启权限')
//     //           console.log(error)
//     //           // wx.navigateTo({
//     //           //   url: "/pages/index/index",
//     //           // });
//     //         },
//     //       })
//     //     } else {
//     //       // 获取 code 失败
//     //       that.showInfo('获取 code 失败')
//     //       console.log('调用wx.login获取code失败')
//     //     }
//     //   },
//     //
//     //   fail: function (error) {
//     //     // 调用 wx.login 接口失败
//     //     that.showInfo('调用 wx.login 接口失败')
//     //     console.log(error)
//     //   },
//     // })
//   },