const config = require('../../../xci/config')
const tempId = 'vr303yq4QmFYAHxdyIpeFOrXhvXZjEdwRgWX_nRH_bc'

Component({
  properties: {},
  data: {
    name: '',
    userInfo:null
  },
  lifetimes: {
    attached: function () {
      const self = this;
      wx.getSetting({
        success(res) {
          console.log(res);
          const sname = 'scope.userInfo'
          if (res.authSetting[sname]) {
            wx.getUserInfo({
              success: function(res) {
                self.setData({
                  userInfo:res.userInfo
                })
              }
            })
          }
        }
      })

    },
    moved: function () {
    },
    detached: function () {
    },
  },
  methods: {
    bindGetUserInfo: function (e) {
      if (e.detail.userInfo) {
        console.log("用户允许", e.detail.userInfo);
         this.setData({
           userInfo:e.detail.userInfo
         })
      }
    },
    showInfo: function (title) {
      wx.showToast({
        title: title
      })
    },
    onSendMessageClick() {
      const that = this;

      wx.requestSubscribeMessage({
        tmplIds: [tempId],
        success(res) {
          console.log(res);
          if (res[tempId] == "accept") {
            wx.showToast({
              title: '订阅成功',
              duration: 1000,
            })
            // 成功调起下发订阅后调后端接口
            //that .getsubscribeMessage(openid)

            wx.login({
              success: function (loginRes) {
                if (loginRes.code) {
                  wx.getUserInfo({
                    success: function (infoRes) {
                      wx.request({
                        url: `${config.baseUrl}/wx/user/${config.wxId}/login`,
                        method: "POST",
                        data: {
                          code: loginRes.code,
                          rawData: infoRes.rawData, // 用户非敏感信息
                          signature: infoRes.signature, // 签名
                          encryptedData: infoRes.encryptedData, // 用户敏感信息
                          iv: infoRes.iv // 解密算法的向量
                        },
                        success: function (res) {
                          wx.request({
                            url: `${config.baseUrl}/wx/user/${config.wxId}/send`,
                            method: 'POST',
                            data: {
                              openid: res.data.openid,
                              page: '/pages/module-test/send-result/send-result',
                              templateId: tempId
                            },
                            success: function (res) {
                              console.log("send success:", res);
                              //that.showInfo("订阅消息发送成功");
                            },

                            fail: function (error) {
                              // 调用服务端登录接口失败
                              that.showInfo("调用发送订阅消息接口失败");
                              console.log(error);
                            }
                          })
                        },
                        fail(err) {
                          console.log(err);
                        }
                      });
                    },
                    fail: function (error) {
                      that.showInfo("获取 userInfo 失败，去检查是否未开启权限");
                      console.log(error);
                    },
                  });
                }
              },
              fail(err) {
                console.log(err);
              }
            })


          } else {
            wx.showModal({
              title: '温馨提示',
              content: '您已拒绝授权，将无法在微信中收到通知！',
            })
          }
        },
        fail(err) {
          console.log(err);
        }
      })
    }
  }
});