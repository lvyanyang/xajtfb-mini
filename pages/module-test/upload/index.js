const helper = require('../../../xci/helper')
let uploadFiles = [];

Page({
  data: {
    uploads: []
  },
  onLoad: function (options) {

  },
  onChangeTap(e) {
    uploadFiles = e.detail.all;
  },
  async onUpload() {
    // const self = this;
    // if (uploadFiles.length === 0) {
    //   await helper.showToast('请选择文件')
    //   return;
    // }
    //
    // for (let i = 0; i < uploadFiles.length; i++) {
    //   const rd = await helper.uploadFile(uploadFiles[i],'mini')
    //   self.setData({
    //     uploads: self.data.uploads.concat(helper.buildUploadFileUrl(rd.data.path))
    //   })
    // }

    const rd = await helper.uploadFiles(uploadFiles)
    console.log(rd)
    const data = []
    rd.forEach(p => data.push(p.data.url))

    this.setData({
      uploads: data
    })
  },
});