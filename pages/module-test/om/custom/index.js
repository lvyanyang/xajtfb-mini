const pagingBehavior = require('../../../../xci/pagingBehavior')
const helper = require('../../../../xci/helper')
const auth = require('../../../../xci/auth')
const service = require('../../../../xci/service')

Page({
  behaviors: [pagingBehavior],
  data: {
    key: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad(options) {
    await auth.loginInterceptor()
    await this.loadData();
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  async onPullDownRefresh() {
    await this.loadData();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  async onReachBottom() {
    await this.loadData(false);
  },

  /**
   * 加载数据
   * @param firstPage 是否加载第一页数据
   */
  async loadData(firstPage = true) {
    await this.loadPageData(service.customer.selectPageList, {}, firstPage)
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})