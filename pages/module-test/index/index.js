const app = getApp()

Page({
  data: {
    namev:'',
    grids: [{
      image: 'order',
      text: '公交线路',
      url: '../bus/line/index'
    }, {
      image: 'cart',
      text: '客户档案',
      url: '../om/custom/index'
    },  {
      image: 'order',
      text: '文件上传',
      url: '../uploadFile/index'
    }, {
      image: 'customer-service',
      text: '微信登录',
      url: '../user/index'
    },{
      image: 'eye',
      text: '我的足迹'
    }, {
      image: 'scan',
      text: '扫一扫'
    }, {
      image: 'address',
      text: '获取定位'
    }, {
      image: 'comment',
      text: '订阅消息',
      url: '../subscribe-message/subscribe-message'
    }, {
      image: 'setting',
      text: '权限设置'
    },]
  },
  onLoad: function () {
  },
  // clickGridItem(e) {
  //   if (e.detail.cell.text === '扫一扫') {
  //     this.onScanCode();
  //     return;
  //   } else if (e.detail.cell.text === '获取定位') {
  //     this.onLocation();
  //     return;
  //   } else if (e.detail.cell.text === '权限设置') {
  //     this.onPermissionSetting();
  //     return;
  //   }
  //
  //   const url = e.detail.cell.url;
  //   if (!url) return;
  //   wx.navigateTo({
  //     url: url
  //   });
  // },

  onScanCode() {
    wx.scanCode({
      success(res) {
        console.log(res)
        wx.showToast({
          icon: 'none',
          title: res.result
        })
      }
    })
  },

  onPermissionSetting() {
    wx.openSetting({
      success(res) {
        console.log(res.authSetting)
        // res.authSetting = {
        //   "scope.userInfo": true,
        //   "scope.userLocation": true
        // }
      }
    })
  },

  onLocation() {
    const self = this;
    wx.getSetting({
      success(res) {
        console.log(res.authSetting);
        if (!res.authSetting['scope.userLocation']) {
          wx.authorize({
            scope: 'scope.userLocation',
            success() {
              self.onLocationCore();
            },
            fail(err) {
              console.log(err);
              wx.showToast({
                title: '用户已拒绝'
              })
            }
          })
        } else {
          self.onLocationCore();
        }
      }
    })
  },
  onLocationCore() {

    wx.getLocation({
      type: 'wgs84',
      fail(err) {
        console.log(err);
      },
      success(res) {
        const latitude = res.latitude
        const longitude = res.longitude
        const speed = res.speed
        const accuracy = res.accuracy
        wx.showModal({
          content: `latitude=${latitude};longitude=${longitude};speed=${speed};accuracy=${accuracy}`
        })
      }
    })

    // wx.chooseLocation({
    //   success(res){
    //     console.log(res);
    //   }
    // })

    // wx.getLocation({
    //   type: 'gcj02', //返回可以用于wx.openLocation的经纬度
    //   success (res) {
    //     const latitude = res.latitude
    //     const longitude = res.longitude
    //     wx.openLocation({
    //       latitude,
    //       longitude,
    //       scale: 18
    //     })
    //   }
    // })

  },


})
