const helper = require('../../../xci/helper')
const auth = require('../../../xci/auth')

Page({
  data: {
    title: '交通互动',
    userInfo: {},
    items: [
      {
        title: '寻人找物',
        icon: 'https://www.xadjtfb.com/Assets/images/czserchIco.png',
        url: '/pages/webview/index?url=http%3a%2f%2fwww.xluob.com%2fmini%2fh5%2findex.html%23%2fall%2findex'
      },
      {
        title: '互动交流',
        icon: 'https://www.xadjtfb.com/Assets/images/czfwpjhdIco.png',
        url: '/pages/webview/index?url=http://36.133.106.227:8001/orderSubmit'
      },
      {title: '出租投诉评价', icon: 'https://www.xadjtfb.com/Assets/images/czfwpjIco1.png', url: '/pages/module-traffic-interaction/complaint/complaint'},
      {title: '我要投稿', icon: 'https://www.xadjtfb.com/Assets/images/czwytgIco.png', url: '/pages/module-traffic-interaction/contribute/contribute'},
    ]
  },
  async onLoad(options) {
    await auth.loginInterceptor()
  },
  onShow() {
    const userInfo = auth.getTokenInfo()
    this.setData({userInfo})
  }
})
