// pages/module-traffic-interaction/evaluate/evaluate.js

const service = require('../../../xci/service')
const insert = service.bus.insertWxEvaluation;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    CarNumber:null,
    CertNo:null,
    CrcmStar:1,
    FwlyStar:4,
    XcaqStar:2,
    ZhpjStar:3,
    Context:null,
    array: ['陕AT', '陕AU', '陕AD'],
    list: [],
    objectArray: [
      {
        id: 0,
        name: '陕AT'
      },
      {
        id: 1,
        name: '陕AU'
      },
      {
        id: 2,
        name: '陕AD'
      }
    ],
    index: 0
  },
  onLoad: function (options) {

  },
  bindPickerChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      index: e.detail.value
    })
  },
  // linChange: function (e) {
  //   if(e.target.id=="crcm"){
  //     this.data.CrcmStar= e.detail.score
  //     console.log('车容车貌值为：',e.detail.score)
  //   }else if(e.target.id=="fwly"){
  //     this.data.FwlyStar= e.detail.score
  //     console.log('服务礼仪值为：',e.detail.score)
  //   }else if(e.target.id=="xcaq"){
  //     this.data.XcaqStar= e.detail.score
  //     console.log('行车安全值为：',e.detail.score)
  //   }else if(e.target.id=="zhpj"){
  //     this.data.ZhpjStar= e.detail.score
  //     console.log('综合评价值为：',e.detail.score)
  //   }
  //   // console.log(e)
  // },

  /**
   * 提交数据
   */
  formSubmit: function (e) {
    console.log('form发生了submit事件，携带数据为：', e)
    console.log('车号：', this.data.CarNumber)
    console.log('司机姓名：', this.data.CertNo)
    console.log('车容车貌值为：', this.data.CrcmStar)
    console.log('服务礼仪值为：', this.data.FwlyStar)
    console.log('行车安全值为：', this.data.XcaqStar)
    console.log('综合评价值为：', this.data.ZhpjStar)
    console.log('评价内容为：', this.data.Context)

    var datas={
      carnumber:this.data.array[this.data.index]+e.detail.value.carnumber,
      certno:e.detail.value.certno,
      crcmstar:this.data.CrcmStar,
      fwlystar:this.data.FwlyStar,
      xcaqstar:this.data.XcaqStar,
      zhpjstar:this.data.ZhpjStar,
      context:e.detail.value.ontext,
      userid:123
    }
    insert(datas);

  },
  loadData() {

    // insert()

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
