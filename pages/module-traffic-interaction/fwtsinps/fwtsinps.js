// pages/module-traffic-interaction/fwtsinps/fwtsinps.js
var dateTimePicker = require('./dateTimePicker');
const config = require('../../../xci/config')
const service = require('../../../xci/service')
const insertWxComplaint = service.bus.insertWxComplaint;
const auth = require('../../../xci/auth')
//文件上传（文件）
let uploadFiles = [];
Page({
  /**
   * 页面的初始数据
   */
  data: {
    array: ['陕AT', '陕AU', '陕AD'],
    index: 0,
    date: '2016-09-01',
    time: '12:01',
    date: '2018-10-01',
    time: '12:00',
    dateTimeArray: null,
    dateTime: null,
    dateTimeArray1: null,
    dateTime1: null,
    startYear: 2000,
    endYear: 2050,
    // src: 'https://res.wx.qq.com/wxdoc/dist/assets/img/0.4cb08bb4.jpg',
    // imgs: [],
    // srcs: '',
    //文件上传后 文件的信息
    uploads: [],
    WxComplaint: {
      //车牌号码
      carNumber: '',
      //乘车时间
      takeTaxiTime: '',
      //用户主键(默认先塞1，等后面注册功能做好后 在塞值)
      userId: '',
      //乘车地点
      takeTaxiPlace: '',
      //投诉大类
      target: '',
      //投诉小类
      type: '',
      //投诉内容
      context: '',
      //附件（图片）
      uploadImgs: ''
    }
  },
  bindPickerChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      index: e.detail.value
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    
    //投诉大类
    this.data.WxComplaint.target = options.target || "";
    //投诉小类
    this.data.WxComplaint.type = options.type || "";

    // 获取完整的年月日 时分秒，以及默认显示的数组
    var obj = dateTimePicker.dateTimePicker(this.data.startYear, this.data.endYear);
    var obj1 = dateTimePicker.dateTimePicker(this.data.startYear, this.data.endYear);
    // 精确到分的处理，将数组的秒去掉
    var lastArray = obj1.dateTimeArray.pop();
    var lastTime = obj1.dateTime.pop();
    this.setData({
      dateTime: obj.dateTime,
      dateTimeArray: obj.dateTimeArray,
      dateTimeArray1: obj1.dateTimeArray,
      dateTime1: obj1.dateTime
    });
  },
  changeDate(e) {
    this.setData({
      date: e.detail.value
    });
  },
  changeTime(e) {
    this.setData({
      time: e.detail.value
    });
  },
  changeDateTime(e) {
    this.setData({
      dateTime: e.detail.value
    });
  },
  changeDateTime1(e) {
    this.setData({
      dateTime1: e.detail.value
    });
  },
  changeDateTimeColumn(e) {
    var arr = this.data.dateTime,
      dateArr = this.data.dateTimeArray;

    arr[e.detail.column] = e.detail.value;
    dateArr[2] = dateTimePicker.getMonthDay(dateArr[0][arr[0]], dateArr[1][arr[1]]);

    this.setData({
      dateTimeArray: dateArr,
      dateTime: arr
    });
  },
  changeDateTimeColumn1(e) {
    var arr = this.data.dateTime1,
      dateArr = this.data.dateTimeArray1;

    arr[e.detail.column] = e.detail.value;
    dateArr[2] = dateTimePicker.getMonthDay(dateArr[0][arr[0]], dateArr[1][arr[1]]);

    this.setData({
      dateTimeArray1: dateArr,
      dateTime1: arr
    })
  },
  async submit(event) {
    //文件上传
    if(uploadFiles.length>0){
      // if (uploadFiles.length === 0) {
      //   wx.showToast({
      //     icon: 'none',
      //     title: '请选择文件'
      //   })
      //   return Promise.resolve({});
      // }
      for (let i = 0; i < uploadFiles.length; i++) {
        const result = await this.uploadFile(uploadFiles[i])
        //console.log("文件上传返回值",result);
        this.setData({
          uploads: this.data.uploads.concat(result.path)
        })
      }
    }
    //数据校验
    // 合法性的验证（车牌号）
    if (!event.detail.value.carNumber.trim() || event.detail.value.carNumber.length!==4) {
      // 不合法
      wx.showToast({
        title: '请输入正确车牌号',
        icon: 'none',
        mask: true
      });
      return;
    }
    //合法性的验证（乘车地点）
    if (!event.detail.value.takeTaxiPlace.trim() ) {
      // 不合法
      wx.showToast({
        title: '请输入乘车地点',
        icon: 'none',
        mask: true
      });
      return;
    }
    //合法性的验证（投诉内容）
    if (!event.detail.value.context.trim() ) {
      // 不合法
      wx.showToast({
        title: '请输入投诉内容',
        icon: 'none',
        mask: true
      });
      return;
    }
    //车牌号码
    this.data.WxComplaint.carNumber=this.data.array[this.data.index]+event.detail.value.carNumber;
    //乘车时间
    this.data.WxComplaint.takeTaxiTime=
        this.data.dateTimeArray1[0][this.data.dateTime1[0]]+"-"+
        this.data.dateTimeArray1[1][this.data.dateTime1[1]] +"-"+
        this.data.dateTimeArray1[2][this.data.dateTime1[2]] +" "+
        this.data.dateTimeArray1[3][this.data.dateTime1[3]] +":"+
        this.data.dateTimeArray1[4][this.data.dateTime1[4]] ;
    //用户主键(默认先塞1，等后面注册功能做好后 在塞值)
    //合法性的验证（userId）
    const appid = auth.getAppId();
    //console.log(appid)
    if (!appid.trim()) {
      // 不合法
      wx.showToast({
        title: '请登录后在投诉',
        icon: 'none',
        mask: true
      });
      return;
    }
    this.data.WxComplaint.userId = appid;
    //乘车地点
    this.data.WxComplaint.takeTaxiPlace=event.detail.value.takeTaxiPlace||"";
    //投诉内容
    this.data.WxComplaint.context=event.detail.value.context||"";
    //文件路径
    this.data.WxComplaint.uploadImgs= this.data.uploads.join(',');
    insertWxComplaint(this.data.WxComplaint);
    wx.navigateBack({
      delta: 3
    });

  },

  uploadFile(path) {
    return new Promise((resolve, reject) => {
      wx.uploadFile({
        url: `${config.baseUrl}/file/upload`,
        filePath: path,
        name: 'file',
        formData: {
          'virtualCatalog': ''
        },
        success(res) {
          const rd = JSON.parse(res.data)
          resolve(rd.data)
        },
        fail(err) {
          console.log(err);
          reject(err);
        }
      })
    })
  },
  onChangeTap(e) {
    uploadFiles = e.detail.all;
  }
})