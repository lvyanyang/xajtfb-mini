Page({
  data: {
    array: [{
    //   message: '出租评价',
    //   srcself: '/static/tab/complaint-cz.png',
    //   url: '../evaluate/evaluate'
    // }, {
      message: '服务投诉',
      srcself: '/static/tab/complaint-fw.png',
      url: '../fwts/fwts'
    },{
      message: '投诉查询',
      srcself: '/static/tab/complaint-ts.png',
      url: '../query/query'
    }
  ]
  },
  onLoad: function (options) {

  },
  clickGridItem(e) {
    const item = e.currentTarget.dataset.itemName;
    const url = item.url;
    if (!url) return;
    wx.navigateTo({
      url: url
    });
  }
});