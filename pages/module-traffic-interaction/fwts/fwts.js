// pages/module-traffic-interaction/fwts/fwts.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    checked:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  onChange(e){
      console.log(e)
      this.setData({
        checked:e.detail.value.length>0,
      })
  },
  onNext(){
    console.log(this.data.checked);
    wx.navigateTo({
      url: '../fwtsinp/fwtsinp'
    });
  }
})