const service = require('../../../xci/service')
const pagingBehavior = require('../../../xci/pagingBehavior')
const auth = require('../../../xci/auth')

Page({
  behaviors: [pagingBehavior],
  /**
   * 页面的初始数据
   */
  data: {
  },

  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad(options) {
    await this.loadData();
  },
  /**
   * 加载数据
   * @param firstPage 是否加载第一页数据
   */
  async loadData(firstPage = true) {
    //console.log(auth.getAppId());
    const userId = auth.getAppId();
    await this.loadPageData(service.bus.selectWxComplaintPageList, {userId}, firstPage)
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  async onPullDownRefresh() {
    await this.loadData();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  async onReachBottom() {
    await this.loadData(false);
  },
})