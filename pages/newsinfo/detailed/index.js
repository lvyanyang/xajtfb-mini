// pages/newsinfo/detailed/index.js
const service = require('../../../xci/service')
Page({
  /**
   * 页面的初始数据
   */
  data: {
    wxNewsinformation:null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.firstloadData(options.id);
    
  },
  firstloadData:function(id){
    service.bus.selectWxNewsinformationById({id:id}).then(res => {
      //console.log(res);
      this.setData({
        wxNewsinformation: res.data
      })
    })
  }

})