const service = require('../../xci/service')
Page({
  /**
   * 页面的初始数据
   */
  data: {
    newsinfosrows:[{
      "id": 19,
      "infoclass": "默认数据",
      "newsinfo": "经上级部门批准同意，238路将于2017年元月8日，由西华门缩线至张家堡西（盐张村）。",
      "adddate": "2017-01-10 18:44:23",
      "roadstate": null,
      "weather": null,
      "emergency": null,
      "classs": "新闻资讯",
      "addoper": null,
      "releasesign": null,
      "releaseoper": null,
      "releasedate": null
    },
    {
      "id": 16,
      "infoclass": "春节期间ETC通行温馨提示",
      "newsinfo": "2017年春节期间7座以下（含7座）小型客车免费通行高速公路时间为：1月27日零点开始，至2月2日24点结束，共7天。免收通行费时间以车辆驶离高速公路出口收费车道的时间为准。",
      "adddate": "2017-01-10 18:27:40",
      "roadstate": null,
      "weather": null,
      "emergency": null,
      "classs": "新闻资讯",
      "addoper": null,
      "releasesign": null,
      "releaseoper": null,
      "releasedate": null
    }]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   this.loadWxNewsinformationPageListData();
  },

  loadWxNewsinformationPageListData:function(){
    service.bus.selectWxNewsinformationPageList({
      pageIndex: 1,
      pageSize: 1000,
      classs: "新闻资讯",
    }).then(res => {
      //根据输入的数据去查--》渲染到下面
      //console.log(res);
      this.setData({
        newsinfosrows: res.data.rows
      })
    })
  }
})