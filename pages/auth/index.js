const auth = require('../../xci/auth')

Page({
  data: {},
  onLoad() {
  },
  /**
   * 授权
   */
  async auth() {
    await auth.register()
    wx.navigateBack()
    // try {
    //   const result = await helper.getOpenId('正在请求授权...')
    //   app.globalData.userInfo = result.data
    //   wx.navigateBack()
    // }
    // catch (e) {
    //    console.log(e)
    // }
  }
})